/* ************************************************************************

   Copyright:
     2010 Guilherme R. Aiolfi

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors: Guilherme R. Aiolfi (guilhermeaiolfi)

************************************************************************ */
qx.Theme.define('tokenfield.theme.cosmetosafe.Appearance',
{
  extend: qx.theme.indigo.Appearance,
  appearances:
  {
    token: 'textfield',
    tokenitem:
    {
      include: 'listitem',
      style: function(states) {
        return {
          backgroundColor: states.hovered ? '#e30808' : '#efefef',
          textColor: 'black',
          height: 18,
          padding: [1, 6, 1, 6],
          margin: [0, 0, 0, 1],
          icon: states.close ? 'cosmetosafe/nok.png' : 'cosmetosafe/nok_grey.png'
        };
      }
    }
  }
});
